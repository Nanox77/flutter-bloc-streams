import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CounterView extends StatelessWidget {
  final Stream<int> counter;

  CounterView(this.counter);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: 0,
      stream: counter,
      builder: (_, snap) {
        return Text(
          '${snap.data}',
          style: Theme.of(context).textTheme.display1,
        );
      },
    );
  }
}
