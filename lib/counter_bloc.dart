import 'dart:async';

import 'package:streams/counter_provider.dart';

class CounterBloc {
  final _counterProvider = CounterProvider();
  final _counterController = StreamController<int>();

  //Input
  StreamSink<int> get _add => _counterController.sink;

  //Output
  Stream<int> get getCounter => _counterController.stream;

  void increment() {
    _counterProvider.increaseCount();
    _add.add(_counterProvider.count);
  }

  void dispose() {
    _counterController.close();
  }
}
